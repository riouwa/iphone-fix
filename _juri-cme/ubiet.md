---
name: Nyak Ina Raseuki
ttl: Jakarta, 24 Mei 1965
ibu:
bapak:
kakak:
adik:
tk:
sd: SDN 5 Sabang, Aceh
smp:
  - SMPN 1 Sabang, Aceh
sma:
  - SMAN 1 Sabang, Aceh
  - SMAN 29 Jakarta
s1: Institut Kesenian Jakarta, Jakarta Vokal-Musik (1983-1989)
s2: <i>University of Wisconsin-Madison, Wisconsin, U.S.A. Master of Music in Ethnomusicology</i> (1993)
s3: <i>University of Wisconsin-Madison, Wisconsin, U.S.A. Ph.D in Ethnomusicology</i> (2009)
afiliasi:
  - Institut Kesenian Jakarta Pascasarjana (2014)
  - Dewan Kesenian Jakarta, Anggota Komite Musik (2006-2009)
  - Taman Musik Dian Indonesia, Direktur (2003-2011)
  - Pendidikan Seni Nusantara, Sekretaris Jendral (2002-2006)
  - <i>University of Wisconsin-Madison, U.S.A</i>, Asisten Pustakawan (1991-1993)
karya:
  - <i>Commonality-The New Jakarta Ensemble</i> (Siam Records, New York, 1999)
  - <i>Archipelagongs</i> (Warner Music Indonesia, Jakarta, 2000)
  - <i>Music for Solo Performer, Ubiet Sings Tony Prabowo</i> (Musikita, Jakarta, 2006)
  - <i>Ubiet Kroncong Tenggara</i> (Ragadi Music/demajors, Jakarta, 2007/2013)
penghargaan:
  - Beasiswa Yayasan Malem Putra-Aceh, 1991, 2000
  - <i>Leland Coon Fellowship, University of Wisconsin-Madison</i>, 1992-1993
  - <i>Indonesian Cultural Foundation Fellowship-New York</i>, 1993
  - <i>The Ford Foundation Fellowship</i>, 1998-2008
  - <i>Center for Southeast Asian Studies Fellowship, University of Wisconsin-Madison<i>, 1998-1999, 2009
  - <i>Asian Cultural Council Fellowship-New York</i>, 1999, 2009, 2011
  - Beasiswa Unggulan, Departemen Pendidikan Nasional, 2008
kolaborasi:
  - Ubiet & Dian HP, <i>Komposisi Delapan Cinta</i> (demajors, 2011)
  - <i>Kain Sigli, in Patahan, simakDialog</i> (demajors, Jakarta, 2006)
  - <i>Jalan Cahaya, in Romantic Rhapsody</i>, ADA Band (EMI Indonesia, 2006)
  - <i>Rhythm of Reformation-KrakatauEthno</i> (Musikita, 2005)
  - <i>2 Worlds-KrakatauEthno</i> (Musikita, Jakarta, 2005)
  - <i>Malacca Bay</i>, in Home, Dewa Budjana (Sony BMG Indonesia, 2005)
  - <i>9 Januari</i>, in Nusa Damai, Dewa Budjana (Chico & Ira, Jakarta, 1997)
riset:
  - <i>Musik Aceh</i> (musik vokal terkait dengan Islam; <i>vocal music associated with Islam</i>)
  - <i>Musik Kerinci</i>, Jambi (musik vokal terkait dengan Islam)
  - <i>Vokal Musik Nusantara, Musik Populer Indonesia</i>
teater:
sutradara:
wawancara:
  -
    judul: "Nyak Ina Raseuki (Ubiet): Ragam Suara dan Sikap dalam Kehidupan"
    link: /ubiet/ragam-suara-dan-sikap-dalam-kehidupan/
foto: /static/img/juricme/ubiet.png
layout: people
---

Lebih suka menyebut dirinya pesuara daripada penyanyi telah menjelajahi beragam gaya musik dan menyatakan dirinya “pluralis”. Lahir dari Ayah Aceh dan Ibu Minangkabau, Ubiet remaja bergabung dengan sejumlah band pop di Sabang (Pulau Weh) dan Banda Aceh. Masuk Jurusan Musik pada Institut Kesenian Jakarta 1983, studi khusus vokal; Ubiet kerap tampil dalam sejumlah pentas musik klasik maupun musik untuk pertunjukan tari dan teater, dan menjadi pesuara kelompok jazz *Splash Band*. Bergelar doktor dalam etnomusikologi (2009) dari *University of Wisconsin—Madison*, Amerika Serikat, Ubiet kerap menyanyikan beberapa karya komposer kontemporer Tony Prabowo yang diciptakan khusus untuknya. Karya ini kemudian dipersembahkan dalam panggung musik dan rekaman, film, teater, tari, peragaan busana, dan pembukaan pameran seni rupa. Sejak 2004, ia terlibat pada kelompok jazz Krakatau-Etno, sebagai pesuara, dan menghasilkan dua album *Rhythms of Reformation* (Musikita, 2005) dan *2 Worlds* (Musikita, 2005). Album solonya termasuk *Archipelagongs* (Warner Music Indonesia, 2000), *Music for Solo Performer: Ubiet Sings Tony Prabowo* (Musikita, 2006), *Ubiet & Kroncong Tenggara* (demajors, 2007/2013), *Komposisi Delapan Cinta* (demajors, 2011).
