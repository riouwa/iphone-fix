---
title: "Andy Yentriyani: Tidak Boleh Menyerah Mengubah Keadaan Menjadi Lebih Indah"
date: 2018-01-05
categories:
- CME
- Wawancara Juri
permalink: /andy-yentriyani/judul-artikel/
img: /static/img/juricme/andy.png
author: siska
comments: true
---


![andy yentriyani](/static/img/juricme/andy.png "andy"){: .img-responsive .center-block }

Disekolahkan di TK Bhayangkari, Pontianak, Kalimantan Barat Andy diajarkan bahwa Indonesia adalah rumah untuk semua orang dengan latar belakang, suku, agama, ras yang berbeda-beda dari Sabang sampai Merauke. Negara yang digambarkan untuk Andi kecil adalah Indonesia negara yang indah saling gotong-royong membangun tataran hidup yang saling mencerdaskan dan mensejahterakan semua orang. Andi kecil menghayati hal tersebut sebagai bebas berjalan dimanapun tanpa ditanya asal usul, jenis kelamin, warna kulit; bebas berkarya dan berusaha selama tujuannya membangun tataran hidup yang lebih baik. Saat cita-cita anak-anak seumurannya adalah menjadi dokter, pilot, maka cita-cita Andy lebih abstrak lagi, yaitu menjadi anak yang berguna bagi bangsa, negara, dan agama.

Masuk SMP, Andi remaja terganggu dengan “hilang”-nya beberapa teman-teman seumuran. “*Cina miskin dikawinin sama orang Taiwan*,” ujarnya, yang merupakan fenomena umum yang terjadi di Pontianak. Daerah kelahirannya ini merupakan wilayah yang cukup beragam, penduduknya memiliki agama yang berbeda-beda, semasa Andy SMP dahulu perkawinan anak adalah kejadian biasa. Sementara Andy remaja terus bertanya tanya “Ada apa sih? Kenapa? Mengapa hal ini terjadi? Mengapa mereka harus pergi dikawinkan di usia muda dan rata-rata kawin dengan orang yang usianya dua kali lipat lebih tua?”, pikirnya. Yang lebih mengganggu lagi, saat Andy resah, ia melihat sekelilingnya tidak resah dan merasa hal tersebut ‘biasa aja’, “Toh setelah kawin bisa kirim uang,” biasanya adalah jawaban yang ia dapatkan. Keresahan ini ia bawa terus selepas SMA, saat masuk Universitas. Memilih untuk menjadi diplomat saat melihat Ali Alatas muncul di TVRI menjelaskan peristiwa berdarah Timor Timur, Santa Cruz, Andy berpikir menjadi diplomat *keren* juga. Iapun mengambil jurusan Hubungan Internasional, Universitas Indonesia. Dalam skripsinya Andy menulis mengenai perkawinan transnasional sebagai satu konsekuensi tata kelola internasional yang menimbulkan ketimpangan antarnegara dan ketimpangan di masyarakat itu sendiri. Saat digali dengan data, ternyata dalam satu tahun bisa ratusan perempuan yang dinikahkan menggunakan modus seperti Tenaga Kerja Wanita (TKW), hanya cara berangkatnya ke luar negerinya saja yang berbeda. “Saat datang di Taiwan, bisa jadi perempuan yang berangkat ternyata tidak kawin dengan orang kaya dan kawin sama-sama dengan petani juga dengan metode tani yang berbeda, mata uang yang berbeda sehingga memungkinkan untuk mengirim uang ke orang tua, sehingga terlihat seperti ekonominya lebih baik. Di Jawa Barat fenomena yang mirip yaitu ‘kawin kontrak’, mengorbankan perempuan, terjadi dengan warga negara Arab,” ungkap Andy. Pada saat menyusunnya Andy mendapatkan tantangan, dipertanyakan apakah benar ini kajian Hubungan Internasional dan bukan kajian Sosiologi? Teknik wawancara kombinasi menggunakan metodologi gender dan perspektif feminis yang pada akhirnya mendapatkan apresiasi yang baik saat itu. Skripsi ini kemudian diangkat menjadi salah satu unggulan untuk S2 bidang Magister Kajian Wanita yang diterbitkan oleh *Ewha Institute Korea*, "Padahal saat itu saya *kan* masih S1 saja," ujarnya.
